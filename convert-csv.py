#!/usr/bin/env python3
# Read ISO-8859-1 semicolon-delimited CSV and output UTF8 comma-delimited CSV
# suitable for hledger
import csv
import io
import sys

wrapper = io.TextIOWrapper(sys.stdin.buffer, encoding='iso-8859-1')
reader = csv.reader(wrapper, delimiter=';')
writer = csv.writer(sys.stdout, delimiter=',', lineterminator='\n')
for row in reader:
    try:
        writer.writerow(row)
    except BrokenPipeError:
        # ignore SIGPIPE (output is piped, but not all of it is read)
        break
