Tools to use [Osuuspankki]() CSV account statements with [hledger]().

[Osuuspankki]: https://op.fi/
[hledger]: http://hledger.org/

## Usage

    ./hledger-osuuspankki tapahtumat20170101-20171231.csv [hledger options]
